import React from 'react'
import Link from 'gatsby-link'

const product_10 = require('../../assets/images/product_10.png')
const product_9 = require('../../assets/images/product_9.png')
const product_8 = require('../../assets/images/product_8.png')
const product_7 = require('../../assets/images/product_7.png')
const product_6 = require('../../assets/images/product_6.png')
const product_5 = require('../../assets/images/product_5.png')
const product_4 = require('../../assets/images/product_4.png')
const product_3 = require('../../assets/images/product_3.png')
const product_2 = require('../../assets/images/product_2.png')
const product_1 = require('../../assets/images/product_1.png')
const input_style = {
    border:0,
     color:'Red',
}

const ShopComponent = () => (
  <div>
    <div>
    <div className="container product_section_container">
		<div className="row">
			<div className="col product_section clearfix">

				

				<div className="breadcrumbs d-flex flex-row align-items-center">
					<ul>
						<li><a href="index.html">Home</a></li>
						<li className="active"><a href="index.html"><i className="fa fa-angle-right" aria-hidden="true"></i>Men's</a></li>
					</ul>
				</div>

				

				<div className="sidebar">
					<div className="sidebar_section">
						<div className="sidebar_title">
							<h5>Product Category</h5>
						</div>
						<ul className="sidebar_categories">
							<li><a href="#">Men</a></li>
							<li className="active"><a href="#"><span><i className="fa fa-angle-double-right" aria-hidden="true"></i></span>Women</a></li>
							<li><a href="#">Accessories</a></li>
							<li><a href="#">New Arrivals</a></li>
							<li><a href="#">Collection</a></li>
							<li><a href="#">Shop</a></li>
						</ul>
					</div>

					
					<div className="sidebar_section">
						<div className="sidebar_title">
							<h5>Filter by Price</h5>
						</div>
						<p>
							<input type="text" id="amount" readonly style={input_style}/>
						</p>
						<div id="slider-range"></div>
						<div className="filter_button"><span>filter</span></div>
					</div>

					
					<div className="sidebar_section">
						<div className="sidebar_title">
							<h5>Sizes</h5>
						</div>
						<ul className="checkboxes">
							<li><i className="fa fa-square-o" aria-hidden="true"></i><span>S</span></li>
							<li className="active"><i className="fa fa-square" aria-hidden="true"></i><span>M</span></li>
							<li><i className="fa fa-square-o" aria-hidden="true"></i><span>L</span></li>
							<li><i className="fa fa-square-o" aria-hidden="true"></i><span>XL</span></li>
							<li><i className="fa fa-square-o" aria-hidden="true"></i><span>XXL</span></li>
						</ul>
					</div>

					
					<div className="sidebar_section">
						<div className="sidebar_title">
							<h5>Color</h5>
						</div>
						<ul className="checkboxes">
							<li><i className="fa fa-square-o" aria-hidden="true"></i><span>Black</span></li>
							<li className="active"><i className="fa fa-square" aria-hidden="true"></i><span>Pink</span></li>
							<li><i className="fa fa-square-o" aria-hidden="true"></i><span>White</span></li>
							<li><i className="fa fa-square-o" aria-hidden="true"></i><span>Blue</span></li>
							<li><i className="fa fa-square-o" aria-hidden="true"></i><span>Orange</span></li>
							<li><i className="fa fa-square-o" aria-hidden="true"></i><span>White</span></li>
							<li><i className="fa fa-square-o" aria-hidden="true"></i><span>Blue</span></li>
							<li><i className="fa fa-square-o" aria-hidden="true"></i><span>Orange</span></li>
							<li><i className="fa fa-square-o" aria-hidden="true"></i><span>White</span></li>
							<li><i className="fa fa-square-o" aria-hidden="true"></i><span>Blue</span></li>
							<li><i className="fa fa-square-o" aria-hidden="true"></i><span>Orange</span></li>
						</ul>
						<div className="show_more">
							<span><span>+</span>Show More</span>
						</div>
					</div>

				</div>

				

				<div className="main_content">

					

					<div className="products_iso">
						<div className="row">
							<div className="col">

								

								<div className="product_sorting_container product_sorting_container_top">
									<ul className="product_sorting">
										<li>
											<span className="type_sorting_text">Default Sorting</span>
											<i className="fa fa-angle-down"></i>
											<ul className="sorting_type">
												<li className="type_sorting_btn" data-isotope-option='{ "sortBy": "original-order" }'><span>Default Sorting</span></li>
												<li className="type_sorting_btn" data-isotope-option='{ "sortBy": "price" }'><span>Price</span></li>
												<li className="type_sorting_btn" data-isotope-option='{ "sortBy": "name" }'><span>Product Name</span></li>
											</ul>
										</li>
										<li>
											<span>Show</span>
											<span className="num_sorting_text">6</span>
											<i className="fa fa-angle-down"></i>
											<ul className="sorting_num">
												<li className="num_sorting_btn"><span>6</span></li>
												<li className="num_sorting_btn"><span>12</span></li>
												<li className="num_sorting_btn"><span>24</span></li>
											</ul>
										</li>
									</ul>
									<div className="pages d-flex flex-row align-items-center">
										<div className="page_current">
											<span>1</span>
											<ul className="page_selection">
												<li><a href="#">1</a></li>
												<li><a href="#">2</a></li>
												<li><a href="#">3</a></li>
											</ul>
										</div>
										<div className="page_total"><span>of</span> 3</div>
										<div id="next_page" className="page_next"><a href="#"><i className="fa fa-long-arrow-right" aria-hidden="true"></i></a></div>
									</div>

								</div>

								

								<div className="product-grid">

									

									<div className="product-item men">
										<div className="product discount product_filter">
											<div className="product_image">
												<img src={product_1} alt=""/>
											</div>
											<div className="favorite favorite_left"></div>
											<div className="product_bubble product_bubble_right product_bubble_red d-flex flex-column align-items-center"><span>-$20</span></div>
											<div className="product_info">
												<h6 className="product_name"><a href="single.html">Fujifilm X100T 16 MP Digital Camera (Silver)</a></h6>
												<div className="product_price">$520.00<span>$590.00</span></div>
											</div>
										</div>
										<div className="red_button add_to_cart_button"><a href="#">add to cart</a></div>
									</div>

									

									<div className="product-item women">
										<div className="product product_filter">
											<div className="product_image">
												<img src={product_2} alt=""/>
											</div>
											<div className="favorite"></div>
											<div className="product_bubble product_bubble_left product_bubble_green d-flex flex-column align-items-center"><span>new</span></div>
											<div className="product_info">
												<h6 className="product_name"><a href="single.html">Samsung CF591 Series Curved 27-Inch FHD Monitor</a></h6>
												<div className="product_price">$610.00</div>
											</div>
										</div>
										<div className="red_button add_to_cart_button"><a href="#">add to cart</a></div>
									</div>

									

									<div className="product-item women">
										<div className="product product_filter">
											<div className="product_image">
												<img src={product_3} alt=""/>
											</div>
											<div className="favorite"></div>
											<div className="product_info">
												<h6 className="product_name"><a href="single.html">Blue Yeti USB Microphone Blackout Edition</a></h6>
												<div className="product_price">$120.00</div>
											</div>
										</div>
										<div className="red_button add_to_cart_button"><a href="#">add to cart</a></div>
									</div>

									

									<div className="product-item accessories">
										<div className="product product_filter">
											<div className="product_image">
												<img src={product_4} alt=""/>
											</div>
											<div className="product_bubble product_bubble_right product_bubble_red d-flex flex-column align-items-center"><span>sale</span></div>
											<div className="favorite favorite_left"></div>
											<div className="product_info">
												<h6 className="product_name"><a href="single.html">DYMO LabelWriter 450 Turbo Thermal Label Printer</a></h6>
												<div className="product_price">$410.00</div>
											</div>
										</div>
										<div className="red_button add_to_cart_button"><a href="#">add to cart</a></div>
									</div>

									

									<div className="product-item women men">
										<div className="product product_filter">
											<div className="product_image">
												<img src={product_5} alt=""/>
											</div>
											<div className="favorite"></div>
											<div className="product_info">
												<h6 className="product_name"><a href="single.html">Pryma Headphones, Rose Gold & Grey</a></h6>
												<div className="product_price">$180.00</div>
											</div>
										</div>
										<div className="red_button add_to_cart_button"><a href="#">add to cart</a></div>
									</div>

									

									<div className="product-item accessories">
										<div className="product discount product_filter">
											<div className="product_image">
												<img src={product_6} alt=""/>
											</div>
											<div className="favorite favorite_left"></div>
											<div className="product_bubble product_bubble_right product_bubble_red d-flex flex-column align-items-center"><span>-$20</span></div>
											<div className="product_info">
												<h6 className="product_name"><a href="single.html">Fujifilm X100T 16 MP Digital Camera (Silver)</a></h6>
												<div className="product_price">$520.00<span>$590.00</span></div>
											</div>
										</div>
										<div className="red_button add_to_cart_button"><a href="#">add to cart</a></div>
									</div>

									

									<div className="product-item women">
										<div className="product product_filter">
											<div className="product_image">
												<img src={product_7} alt=""/>
											</div>
											<div className="favorite"></div>
											<div className="product_info">
												<h6 className="product_name"><a href="single.html">Samsung CF591 Series Curved 27-Inch FHD Monitor</a></h6>
												<div className="product_price">$610.00</div>
											</div>
										</div>
										<div className="red_button add_to_cart_button"><a href="#">add to cart</a></div>
									</div>

									

									<div className="product-item accessories">
										<div className="product product_filter">
											<div className="product_image">
												<img src={product_8} alt=""/>
											</div>
											<div className="favorite"></div>
											<div className="product_info">
												<h6 className="product_name"><a href="single.html">Blue Yeti USB Microphone Blackout Edition</a></h6>
												<div className="product_price">$120.00</div>
											</div>
										</div>
										<div className="red_button add_to_cart_button"><a href="#">add to cart</a></div>
									</div>

									

									<div className="product-item men">
										<div className="product product_filter">
											<div className="product_image">
												<img src={product_9} alt=""/>
											</div>
											<div className="product_bubble product_bubble_right product_bubble_red d-flex flex-column align-items-center"><span>sale</span></div>
											<div className="favorite favorite_left"></div>
											<div className="product_info">
												<h6 className="product_name"><a href="single.html">DYMO LabelWriter 450 Turbo Thermal Label Printer</a></h6>
												<div className="product_price">$410.00</div>
											</div>
										</div>
										<div className="red_button add_to_cart_button"><a href="#">add to cart</a></div>
									</div>

									

									<div className="product-item men">
										<div className="product product_filter">
											<div className="product_image">
												<img src={product_10} alt=""/>
											</div>
											<div className="favorite"></div>
											<div className="product_info">
												<h6 className="product_name"><a href="single.html">Pryma Headphones, Rose Gold & Grey</a></h6>
												<div className="product_price">$180.00</div>
											</div>
										</div>
										<div className="red_button add_to_cart_button"><a href="#">add to cart</a></div>
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
  </div>
  </div>
  </div>
)

export default ShopComponent
