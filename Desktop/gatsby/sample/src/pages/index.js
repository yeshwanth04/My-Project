import React from 'react'
import Link from 'gatsby-link'
//onst banner = require('../../assets/images/banner_1.jpg')
import BannerComponent from './indexPageComponents/banner'
import CategoryTypes from './indexPageComponents/categorySlider'
import NewArrivalcomponent from './indexPageComponents/newArrivalsComponent'


const IndexPage = () => (
  <div>
	  {/*<h1>banner contents</h1>*/}
	  <BannerComponent/>
	  <CategoryTypes/>
	  <NewArrivalcomponent/>

  </div>
)

export default IndexPage
