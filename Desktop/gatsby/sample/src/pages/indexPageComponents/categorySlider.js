import React from "react"
const banner1 = require('../../../assets/images/banner_1.jpg')
const banner2 = require('../../../assets/images/banner_2.jpg')
const banner3 = require('../../../assets/images/banner_3.jpg')

const banner1Style = {
    backgroundImage: `url(${banner1})`
}

const banner2Style = {
    backgroundImage: `url(${banner2})`
}

const banner3Style = {
    backgroundImage: `url(${banner3})`
}

const CategoryTypes = ()=>(
    <div>
        <div className="banner">
		<div className="container">
			<div className="row">
				<div className="col-md-4">
					<div className="banner_item align-items-center" style={banner1Style}>
						<div className="banner_category">
							<a href="categories.html">category1</a>
						</div>
					</div>
				</div>
				<div className="col-md-4">
					<div className="banner_item align-items-center" style={banner2Style}>
						<div className="banner_category">
							<a href="categories.html">category2</a>
						</div>
					</div>
				</div>
				<div className="col-md-4">
					<div className="banner_item align-items-center" style={banner3Style}>
						<div className="banner_category">
							<a href="categories.html">category3</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    </div>
)

export default CategoryTypes