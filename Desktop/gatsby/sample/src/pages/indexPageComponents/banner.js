import React from 'react'
import Link from 'gatsby-link'
const bannerBackground = require('../../../assets/images/slider_1.jpg')

const backGroundStyle = {
    backgroundImage: `url(${bannerBackground})`
}
const BannerComponent = () => (
  <div>
	  {/*<h1>banner contents</h1>*/}
	  <div className="main_slider" style = {backGroundStyle} >
		<div className="container fill_height">
			<div className="row align-items-center fill_height">
				<div className="col">
					<div className="main_slider_content">
						<h6>Banner heading goes here</h6>
						<h1>Any exciting offers go in this banner</h1>
						<div className="red_button shop_now_button"><a href="#">shop now</a></div>
					</div>
				</div>
			</div>
		</div>
	</div>



  </div>
)

export default BannerComponent

//"background-image:url(images/slider_1.jpg)"